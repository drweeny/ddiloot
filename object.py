
class Loot(object):

    def __init__(self):
        pass

    @property
    def location(self):
        '''
        get a description from keywords
        Returns:

        '''
        pass

    @property
    def magic(self):
        '''
        get magic
        Returns:

        '''

    @property
    def theme(self):
        '''
        get themes
        `vampire, fire ghost, XVIII`
        Returns:

        '''

    @property
    def benefits(self):
        '''

        Returns:

        '''
        pass

    @property
    def cost(self):
        '''
        calculate the gold price
        Returns:

        '''

    @property
    def rent(self):
        '''

        Returns:

        '''
        pass

    @property
    def level(self):
        '''
        should be set to get the scale of everything
        should be defined by : level number, power (minor, major)
        Returns:

        '''
        pass

    @property
    def history(self):
        '''
        has it belongs to someone ?
        should be really rare characteristics
        Returns:

        '''
        pass

    @property
    def type(self):
        '''
        get type of object : parchment, spell, object, armor, sword
        Returns:

        '''
        pass

    @property
    def material(self):
        '''
        silver, gold , iron, mithril, admantium, mud
        Returns:

        '''
        pass

    @property
    def creatures(self):
        '''
        find a mog, unicorn, really rare
        Returns:

        '''



